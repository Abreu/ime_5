# Prova de Transferência

Resolução, em linguagem C, dos exercícios de programação propostos na prova de Transferência Interna para o curso de Ciência da Computação da Universidade de São Paulo, em 2023.

## Exercício 1 ([primeMultiples.c](https://git.disroot.org/Abreu/ime_5/src/branch/main/primeMultiples.c))

Dado um número inteiro qualquer, desenvolva uma função, ou conjunto de funções, que apresente pares de números primos cujo múltiplo seja seu divisor.

## Exercício 2 ([mergeUnique.c](https://git.disroot.org/Abreu/ime_5/src/branch/main/mergeUnique.c))

Dadas duas sequências de números inteiros em ordem não decrescente, desenvolva uma função, ou conjunto de funções, que as intercale em ordem crescente em uma nova sequência, sem números repetidos.

## Exercício 3 ([spiralMatrix.c](https://git.disroot.org/Abreu/ime_5/src/branch/main/spiralMatrix.c))

Dados dois números inteiros n e m, percorra uma matrix de dimensões n x m populando-a em espiral com valores inteiros de 1 à n x m.

