#include <malloc.h>
#include <printf.h>
#include <stdio.h>

int *readArray(int *size) {
    int d, i = 0, *A;
    char c;

    *size = 1;
    A = malloc(sizeof(int));
    do {
        if (!scanf(" %d", &d))
            continue;
        if (i == *size - 1) {
            *size *= 2;
            A = realloc(A, *size * sizeof(int));
        }
        A[i++] = d;
    } while ((c = getchar()) != EOF && c != '\n');
    *size = i;
    return A;
}

int *mergeUnique(int *a, int sizeA, int *b, int sizeB, int *size) {
    int i, j, k, *result;

    i = j = k = 0;
    *size = sizeA + sizeB;
    result = malloc(*size * sizeof(int));
    result[k++] = (a[i] <= b[j]) ? a[i++] : b[j++];

    while (i < sizeA && j < sizeB) {
        result[k] = (j == sizeB || a[i] <= b[j]) ? a[i++] : b[j++];
        if (result[k] > result[k - 1])
            k++;
    }
    while (i < sizeA) {
        result[k] = a[i++];
        if (result[k] > result[k - 1])
            k++;
    }
    while (j < sizeB) {
        result[k] = b[j++];
        if (result[k] > result[k - 1])
            k++;
    }
    *size = k;
    return result;
}

void printArray(int *a, int size) {
    int i;

    for (i = 0; i < size; i++)
        printf("%d ", a[i]);
    printf("\n");
}

int main(int argc, char *argv[]) {
    int *a, sizeA, *b, sizeB, *c, sizeC;
    printf("Digite uma sequência de números inteiros em ordem não decrescente "
           "e pressione ENTER:\n");
    a = readArray(&sizeA);
    printf("Digite outra sequência tal:\n");
    b = readArray(&sizeB);
    printf("Sequência da intercalação de com números únicos:\n");
    c = mergeUnique(a, sizeA, b, sizeB, &sizeC);
    printArray(c, sizeC);
    free(a);
    free(b);
    free(c);
    return 0;
}
