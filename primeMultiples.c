#include <malloc.h>
#include <stdbool.h>
#include <stdio.h>

typedef struct node {
    int value;
    struct node *next;
} Stack;

Stack *newStack(int value, Stack *next) {
    Stack *s = malloc(sizeof(Stack));
    s->value = value;
    s->next = next;
    return s;
}

bool isPrime(int value, Stack *primes) {
    if (primes->value == 1)
        return true;
    return (value % primes->value == 0) ? false : isPrime(value, primes->next);
}

Stack *primes(int max) {
    int i;
    Stack *p = newStack(1, NULL);
    p = newStack(2, p);

    for (i = 3; i <= max; i += 2)
        if (isPrime(i, p))
            p = newStack(i, p);
    return p;
}

void freeStack(Stack *s) {
    if (!s)
        return;
    freeStack(s->next);
    free(s);
}

void printPrimeMult(int n) {
    Stack *a, *b, *s = primes(n);

    for (a = s; a; a = a->next)
        for (b = a->next; b; b = b->next)
            if (n % (a->value * b->value) == 0)
                printf("%d x %d\n", a->value, b->value);
    freeStack(s);
}

int main(int argc, char *argv[]) {
    int n;
    printf("Given an integer, this program calculates which combinations of "
           "the multiplications of two primes divides it.\nType in an integer "
           "value: ");
    scanf(" %d", &n);
    printPrimeMult(n);
    return 0;
}
