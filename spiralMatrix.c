#include <stdio.h>
#include <stdlib.h>

void moveRight(int **matrix, int line, int column, int hDist, int vDist,
               int count);
void moveDown(int **matrix, int line, int height, int column, int width,
              int count);
void moveLeft(int **matrix, int line, int height, int column, int width,
              int count);
void moveUp(int **matrix, int line, int height, int column, int width,
            int count);

void moveRight(int **matrix, int line, int column, int hDist, int vDist,
               int count) {
    int distance = hDist;

    if (distance == 0)
        return;

    while (distance-- > 0)
        matrix[line][column++] = ++count;
    moveDown(matrix, line + 1, column - 1, hDist - 1, vDist - 1, count);
}

void moveDown(int **matrix, int line, int column, int hDist, int vDist,
              int count) {
    int distance = vDist;

    if (distance == 0)
        return;
    while (distance-- > 0)
        matrix[line++][column] = ++count;
    moveLeft(matrix, line - 1, column - 1, hDist, vDist, count);
}

void moveLeft(int **matrix, int line, int column, int hDist, int vDist,
              int count) {
    int distance = hDist;

    if (distance == 0)
        return;
    while (distance-- > 0)
        matrix[line][column--] = ++count;
    moveUp(matrix, line - 1, column + 1, hDist - 1, vDist - 1, count);
}

void moveUp(int **matrix, int line, int column, int hDist, int vDist,
            int count) {
    int distance = vDist;

    if (distance == 0)
        return;
    while (distance-- > 0)
        matrix[line--][column] = ++count;
    moveRight(matrix, line + 1, column + 1, hDist, vDist, count);
}

int **createSpiral(int m, int n) {
    int i, **spiral = malloc(m * sizeof(int *));

    for (i = 0; i < m; i++)
        spiral[i] = malloc(n * sizeof(int));

    moveRight(spiral, 0, 0, n, m, 0);
    return spiral;
}

int digits(int i) { return (i < 10) ? 1 : 1 + digits(i / 10); }

void printMatrix(int **spiral, int m, int n) {
    int i, j, d = digits(m * n);

    for (i = 0; i < m; i++) {
        for (j = 0; j < n; j++)
            printf("%*d ", d, spiral[i][j]);
        printf("\n");
    }
}

void freeSpiral(int **s, int m) {
    int i;
    for (i = 0; i < m; i++)
        free(s[i]);
    free(s);
}

int main(int argc, char *argv[]) {
    int m = atoi(argv[1]), n = atoi(argv[2]), **s = createSpiral(m, n);
    printMatrix(s, m, n);
    freeSpiral(s, m);
    return 0;
}
